CC=g++
CFLAGS=-c -Wall -std=c++11 -g `sdl2-config --cflags`
LDFLAGS=-lSDL2 -lSDL2_image -lSDL2_ttf -lSDL2_mixer
TARGET=runme

SOURCES=$(wildcard *.cpp)
SOURCES+=$(wildcard *.c)
OBJECTS=$(SOURCES:.cpp=.o)

all: $(SOURCES) $(TARGET)

# pull inn dependency info for *existing* .o files
DEPS := $(OBJECTS:.o=.d)
-include $(DEPS)

$(TARGET): $(OBJECTS)
	$(CC) $(OBJECTS) $(LDFLAGS) -o $@

%.o: %.cpp
	$(CC) $(CFLAGS) $<
	@$(CC) -MM -MT $@ $(CFLAGS) $< > $*.d
	@cp -f $*.d $*.d.tmp
	@sed -e 's/.*://' -e 's/\\$$//' < $*.d.tmp | fmt -1 | \
	  sed -e 's/^ *//' -e 's/$$/:/' >> $*.d
	@rm -f $*.d.tmp

clean:
	rm -f $(TARGET) *.d *.o
