#include <cmath>
#include <cstdio>
#include <SDL_image.h>
#include "exceptions.h"
#include "gameconfig.h"
#include "gameutils.h"



int GameUtils::getScreenProperty(short d){
    // i can be [1]:letterbox, [2]: 16:10 or [3]: 16:9
    static short i= 0;
    if(i == 0){
        SDL_DisplayMode dm;
        if(SDL_GetDesktopDisplayMode(0, &dm) != 0){
            printf("FAILED TO ACQUIRE DISPLAYMODE:%s\n", SDL_GetError());
            return false;
        }

        // Calculate the ration
        float r = (float) dm.w/(float) dm.h;

        if(r > 1.5 && r < 1.7){
            i = 2; // 16:10
        }
        else if(r > 1.7){
            i = 3; // 16:9
        }
        else {
            i = 1; // 4:3
        }
    }

    if(i == 2){
        if(d == WIDTH_ARG){
            return 800;
        }
        else {
            return 500;
        }
    }

    if(i == 3 ){
        if(d == WIDTH_ARG){
            return 960;
        }
        else {
            return 540;
        }
    }

    else {
        if(d == WIDTH_ARG){
            return 800;
        }
        else {
            return 600;
        }
    }
}

SDL_Texture* GameUtils::loadTexture(SDL_Renderer* renderer, std::string path){
    SDL_Surface* loaded = GameUtils::loadImage(path);
    if(loaded == NULL){
        printf("Failed to create texture, unable to load image\n");
        throw Exception::FAILED_TO_LOAD_SURFACE;
    }
    SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer, loaded);
    SDL_FreeSurface(loaded);
    if(texture == NULL){
        printf("Failed to create texture: %s\n", SDL_GetError());
        throw Exception::FAILED_TO_LOAD_TEXTURE;
    }
    return texture;
}

SDL_Surface* GameUtils::loadImage(std::string path){
    if(!USE_PHYSFS){
        SDL_Surface* loadedSurface = IMG_Load(path.c_str());

        if(loadedSurface == NULL){
            throw Exception::FAILED_TO_LOAD_SURFACE;
            printf("Unable to load the image %s\n%s\n", path.c_str(), IMG_GetError());
        }
        return loadedSurface;
    }
}

int GameUtils::roundToInt(double val){
    double n = floor(val + 0.5);
    return (int) n;
}
