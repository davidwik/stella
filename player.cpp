#include "player.h"
#include "osdir.h"
#include "animation.h"
#include "gameutils.h"

#define TILT 20

void Player::init(std::shared_ptr<Manager> man){
    if(!man->animLib()->has("playerAnimation")){
        printf("Loading player's resources\n");
        std::shared_ptr<Animation> an (new Animation(man->renderer(), 10));
        std::string dir = OSDir::getExecPath() +
            "assets" + OSDir::dirsep() +
            "gfx" + OSDir::dirsep() +
            "ships" + OSDir::dirsep() +
            "player" + OSDir::dirsep();
        an->addFrame(dir + "player001.png");
        an->addFrame(dir + "player002.png");
        an->addFrame(dir + "player003.png");
        man->animLib()->add("playerAnimation", an);
    }
}

Player::Player(GameObject::Type type,
               std::shared_ptr<Manager> man,
               int x, int y) : GameObject(type, man, x, y){
    box.w = 512; //GameUtils::roundToInt(64);
    box.h = 512; //GameUtils::roundToInt(64);

}

Player::~Player(){}

void Player::listen(SDL_Event ev, std::unique_ptr<Camera> &cam){

    if(box.w != 64){
        return;
    }

    const Uint8* currentKeyState = SDL_GetKeyboardState(NULL);
    int pxMove = moveSpeed*2;



    if(currentKeyState[SDL_SCANCODE_UP]){
        if(cam->withinConstraints(box, Camera::Movement::UP)){
            vMovement = Player::Vertical::UP;
            if(!cam->moveCameraUp(box, pxMove)){
                box.y -= pxMove;
            }
        }
    }

    else if(currentKeyState[SDL_SCANCODE_DOWN]){
        if(cam->withinConstraints(box, Camera::Movement::DOWN)){
            vMovement = Player::Vertical::DOWN;
            if(!cam->moveCameraDown(box, pxMove)){
                box.y += pxMove;
            }
        }
    }
    else {
        vMovement = Player::Vertical::NONE;
    }

    if(currentKeyState[SDL_SCANCODE_RIGHT]){
        if(cam->withinConstraints(box, Camera::Movement::RIGHT)){
            hMovement = Player::Horizontal::RIGHT;
            if(!cam->moveCameraRight(box, pxMove)){
                box.x += pxMove;
            }
        }
    }

    if(currentKeyState[SDL_SCANCODE_LEFT]){
        if(cam->withinConstraints(box, Camera::Movement::LEFT)){
            hMovement = Player::Horizontal::LEFT;
            if(!cam->moveCameraLeft(box, pxMove)){
                box.x -= pxMove;
            }
        }
    }
}

void Player::update(){}


void Player::render(){

    if(box.w > 64){
        box.w -= 8;
        box.h -= 8;
    }

    //angle++;
    SDL_Point p;
    p.x = GameUtils::roundToInt(box.w/2);
    p.y = GameUtils::roundToInt(box.h/2);

    SDL_RendererFlip f = (hMovement == Player::Horizontal::LEFT) ? SDL_FLIP_HORIZONTAL : SDL_FLIP_NONE;

    if(vMovement == Player::Vertical::UP){
        angle = (f == SDL_FLIP_NONE) ? -TILT : TILT;
    }
    else if(vMovement == Player::Vertical::DOWN){
        angle = (f == SDL_FLIP_NONE) ? TILT : -TILT;
    }
    else {
        angle = 0;
    }

    SDL_RenderCopyEx(
        manager->renderer(),
        manager->animLib()->get("playerAnimation")->getFrame(),
        NULL,
        &box,
        angle,
        &p,
        f
    );
}
