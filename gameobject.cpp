#include "gameobject.h"

GameObject::GameObject(GameObject::Type type, std::shared_ptr<Manager> man, int x, int y){
    manager = man;
    typeId = type;
    box.x = x;
    box.y = y;
}

GameObject::~GameObject(){}

void GameObject::killObject(){
    isDying = true;
}

void GameObject::setTTL(Uint32 t){
    ttl = t;
}

bool GameObject::shouldIBeDead(){
    if(ttl < SDL_GetTicks() && ttl != 0){
        isDying = true;
    }
    return isDying;
}

GameObject::Type GameObject::getType(void){
    return typeId;
}
