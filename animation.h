#ifndef __ANIMATION_H__
#define __ANIMATION_H__
#include <SDL.h>
#include <vector>
#include <string>
#include "timer.h"


class Animation {

public:
    enum class Status { STOP, PLAY, PAUSE };
    Animation(SDL_Renderer* rend, int framesPerSecond = 5, unsigned int reserve = 20);
    ~Animation();

    void setStatus(Animation::Status st);
    Animation::Status getStatus();
    void addFrame(std::string path, float delay = 1.0);
    SDL_Texture* getFrame(void);


private:
    SDL_Renderer* renderer;
    unsigned int fps;
    unsigned int fps_ms;
    Timer ts;

    Animation::Status status;
    std::vector <SDL_Texture*> frames;
    std::vector <float> frameDelay;
    unsigned int currentFrame;
};

#endif /* __ANIMATION_H__ */
