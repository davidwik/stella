#ifndef __GAMEOBJECT_H__
#define __GAMEOBJECT_H__
#include <SDL.h>
#include <memory>
#include "manager.h"
#include "camera.h"

class GameObject {

public:
    enum class Type {
        PLAYER,
        ENEMY,
        PLAYER_FIRE,
        ENEMY_FIRE,
        BLOCK
    };

    enum class Vertical {
        UP,
        DOWN,
        NONE
    };

    enum class Horizontal {
        LEFT,
        RIGHT,
        NONE
    };

    int zIndex = 0;
    GameObject(GameObject::Type type, std::shared_ptr<Manager> man, int x = 0, int y = 0);

    ~GameObject();

    GameObject::Type getType(void);
    void killObject();
    void setTTL(Uint32 t);
    bool shouldIBeDead();

    virtual void listen(SDL_Event ev, std::unique_ptr<Camera> &c) = 0;
    virtual void update() = 0;
    virtual void render() = 0;

protected:
    GameObject::Type typeId;
    GameObject::Horizontal hMovement = GameObject::Horizontal::NONE;
    GameObject::Vertical vMovement = GameObject::Vertical::NONE;

    SDL_Rect box;
    bool isDying = false;
    Uint32 ttl = 0;
    int angle = 0;
    std::shared_ptr<Manager> manager;


};

#endif /* __GAMEOBJECT_H__ */
