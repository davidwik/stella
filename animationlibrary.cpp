#include "exceptions.h"
#include "animationlibrary.h"

AnimationLibrary::AnimationLibrary(){}

AnimationLibrary::~AnimationLibrary(){
    purge();
}

void AnimationLibrary::purge(){
    printf("Cleaning up the animation library\n");
    library.clear();
}

bool AnimationLibrary::has(std::string key){
    return (library.count(key) > 0) ? true : false;
}

void AnimationLibrary::add(std::string key, std::shared_ptr<Animation> anim){
    if(!has(key)){
        library[key] = anim;
    }
}

void AnimationLibrary::remove(std::string key){
    if(has(key)){
        std::unordered_map<std::string, std::shared_ptr<Animation> >::iterator it;
        it = library.find(key);
        library.erase(it);
    }
}

std::shared_ptr<Animation> AnimationLibrary::get(std::string key){
    if(!has(key)){
        throw Exception::ANIMATION_NOT_FOUND;
    }
    return library[key];
}
