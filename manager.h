#ifndef __MANAGER_H__
#define __MANAGER_H__
#include <SDL.h>
#include <memory>
#include "animationlibrary.h"

/**
 * Manager is a class that wrap essential resources that's needed
 * all around in the game.
 */
class Manager {
private:
    SDL_Renderer* rend;
    SDL_Window* win;
    std::shared_ptr<AnimationLibrary> al;

public:
    Manager(SDL_Window* w, SDL_Renderer *r);
    ~Manager();
    void setAnimLib(std::shared_ptr<AnimationLibrary> a);
    SDL_Renderer* renderer();
    SDL_Window* window();
    std::shared_ptr<AnimationLibrary> animLib();

};



#endif /* __MANAGER_H__*/
