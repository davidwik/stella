#include <cstdio>
#include "menu.h"

Menu::Menu(std::shared_ptr<Manager> man) : GameState(man){
    name = "Menu";
    counter = 0;
    running = true;
    printf("[%s] - Setting up resources\n", name.c_str());
}


void Menu::listen(SDL_Event event){
    printf("[%s] - Is listening to events!\n", name.c_str());
}

void Menu::update(){
    printf("[%s] - Update objects data.\n", name.c_str());
    counter++;
    if(counter > 10){
        running = false;
        GameState::stateId = GameState::State::QUIT;
    }
}

void Menu::render(){
    printf("[%s] - Renders all the data on screen.\n", name.c_str());
}

Menu::~Menu(){
    printf("[%s] - Destroys state\n", name.c_str());
}
