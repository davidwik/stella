#ifndef __OSDIR_H__
#define __OSDIR_H__
#include <string>
#include <iostream>
#include <sstream>
#include <cstdlib>
#include <cstring>
#include <vector>
#include <iterator>
#include <algorithm>
#include <cstdlib>

char dir_sep(void);

std::string _getHomeDir(void);
std::string _getExecPath(void);
std::string _getAppDataFolder(void);
bool _createDirectory(std::string path);
bool _directoryExists(std::string path);

void notSupported(void);

/* Shortens string to a specific delimiter */
std::string _shorten(char delimiter, std::string str);

/* Functions for splitting a string into a vector */
std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems);
std::vector<std::string> split(const std::string &s, char delim);



class OSDir {

public:
    /**
     * Returns the directory seperator for the
     * specific OS.
     * @return const char
     */
    static char dirsep(void);

    /**
     * Returns the path as a string where the executable is located.
     * @return std::string
     */
    static std::string getExecPath(void);

    /**
     * Returns the user's home folder.
     * @return std::string
     */
    static std::string getHomeDir(void);

    /**
     * Returns the application storage folder, like %appData% for windows
     * @return std::string
     */
    static std::string getAppDataFolder(void);

    /**
     * Creates a single directory, won't create directory
     * if underlying directory doesn't exists.
     * @param std::string path
     * @return boolean
     */
    static bool createDirectory(std::string path);

    /**
     * Creates a full path, if diretories doesn't exists it tries to create them.
     * @param std::string path
     * @return boolean
     */
    static bool createPath(std::string path);

    /**
     * Checks whether a directory exists or not
     * @return boolean
     */
    static bool directoryExists(std::string path);

};




#endif /* __OSDIR_H__ */
