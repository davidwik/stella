#ifndef __INTRO_H__
#define __INTRO_H__
#include <memory>
#include "gamestate.h"
#include "manager.h"


class Intro : public GameState {

private:
    int counter;

public:
    void listen(SDL_Event event);
    void update();
    void render();
    Intro(std::shared_ptr<Manager> man);
    ~Intro();

};


#endif /* __INTRO_H__ */
