#include "manager.h"

Manager::Manager(SDL_Window* w, SDL_Renderer *r){
    rend = r;
    win = w;
}

void Manager::setAnimLib(std::shared_ptr<AnimationLibrary> a){
    al = a;
}

SDL_Window* Manager::window(){
    return win;
}

SDL_Renderer* Manager::renderer(){
    return rend;
}

std::shared_ptr<AnimationLibrary> Manager::animLib(){
    return al;
}

Manager::~Manager(){
    rend = NULL;
    win = NULL;
    printf("Manager was successfully removed...\n");
}
