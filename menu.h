#ifndef __MENU_H__
#define __MENU_H__
#include <memory>
#include "gamestate.h"

class Menu : public GameState {

private:
    int counter;

public:
    Menu(std::shared_ptr<Manager> man);
    void listen(SDL_Event event);
    void update();
    void render();
    Menu();
    ~Menu();

};



#endif /* __MENU_H__ */
