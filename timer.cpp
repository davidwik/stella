#include <SDL.h>
#include "timer.h"
#include "gameutils.h"

Timer::Timer(){
    startTime = 0;
    savedTime = 0;
}

void Timer::start(){
    startTime = SDL_GetTicks();
    status = Timer::Status::RUNNING;
}

void Timer::pause(){
    savedTime += SDL_GetTicks() - startTime;
    status = Timer::Status::PAUSED;
}

void Timer::stop(){
    savedTime += SDL_GetTicks() - startTime;
    status = Timer::Status::STOPPED;
}

void Timer::reset(){
    startTime = 0;
    savedTime = 0;
}

Uint32 Timer::getTicks(){
    if(status == Timer::Status::STOPPED || status == Timer::Status::PAUSED){
        return savedTime;
    }
    return (SDL_GetTicks() - startTime) + savedTime;
}
