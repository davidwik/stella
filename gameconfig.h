#ifndef __GAMECONFIG_H__
#define __GAMECONFIG_H__
#include "gameutils.h"

#define FULLSCREEN 1
// DETERMINE WHETHER ITS LETTERBOX OR FULLSCREEN.

#define SCREEN_WIDTH GameUtils::getScreenProperty(WIDTH_ARG)
#define SCREEN_HEIGHT GameUtils::getScreenProperty(HEIGHT_ARG)

#define GAME_TITLE "Stella vorabilis"

#define FPS 30
#define USE_PHYSFS 0
#define SHOW_COLLISION 0



#endif /* __GAMECONFIG_H__ */
