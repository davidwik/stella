#ifndef __ANIMATIONLIBRARY_H__
#define __ANIMATIONLIBRARY_H__
#include <string>
#include <unordered_map>
#include <memory>
#include "animation.h"

class AnimationLibrary {

private:
    std::unordered_map<std::string, std::shared_ptr<Animation> > library;

protected:
    void purge();
    void remove(std::string key);

public:
    AnimationLibrary();
    ~AnimationLibrary();
    bool has(std::string key);
    void add(std::string key, std::shared_ptr<Animation> anim);
    std::shared_ptr<Animation> get(std::string key);
};

#endif /* __ANIMATIONLIBRARY_H__ */
