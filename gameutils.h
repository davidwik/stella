#ifndef __GAMEUTILS_H__
#define __GAMEUTILS_H__
#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"
#include <SDL.h>
#include <string>
#define WIDTH_ARG 0
#define HEIGHT_ARG 1


class GameUtils {

public:
    static int getScreenProperty(short d);
    static SDL_Texture* loadTexture(SDL_Renderer* renderer, std::string path);
    static SDL_Surface* loadImage(std::string path);
    static int roundToInt(double val);
};



#endif /* __GAMEUTILS_H__ */
