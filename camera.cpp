#include "camera.h"
#include "gameutils.h"

Camera::Camera(unsigned int levelWidth,
               unsigned int levelHeight,
               unsigned int cameraWidth,
               unsigned int cameraHeight){
    camera.w = cameraWidth;
    camera.h = cameraHeight;
    level.w = levelWidth;
    level.h = levelHeight;
    level.x = 0;
    level.y = 0;
    camera.x = 0;
    camera.y = GameUtils::roundToInt((levelHeight/2) - (cameraHeight/2));
}


void Camera::setMargins(int top, int bottom, int left, int right){
    margins.top = top;
    margins.bottom = bottom;
    margins.left = left;
    margins.right = right;
}

bool Camera::withinConstraints(SDL_Rect box, Camera::Movement m, int pad){
    if(m == Camera::Movement::UP){
        if(box.y - pad <= 0){
            return false;
        }
        return true;
    }

    if(m == Camera::Movement::DOWN){
        if((box.y + box.h + pad) > camera.h){
            return false;
        }
        return true;
    }

    if(m == Camera::Movement::LEFT){
        if(box.x - pad <= 0){
            return false;
        }
        return true;
    }

    if(m == Camera::Movement::RIGHT){
        if((box.x + box.w*2) > camera.w){
            return false;
        }
        return true;
    }
    return true;
}


bool Camera::moveCameraUp(SDL_Rect box, int px){
    if(box.y < padding){
        return moveUp(px);
    }
    return false;
}

bool Camera::moveCameraDown(SDL_Rect box, int px){
    if(box.y > camera.h-margins.top){
        return moveDown(px);
    }
    return false;
}

bool Camera::moveCameraLeft(SDL_Rect box, int px){
    if(box.x < margins.left){
        return moveLeft(px);

    }
    return false;
}


bool Camera::moveCameraRight(SDL_Rect box, int px){
    if(box.x + margins.right > camera.w){
        return moveRight(px);
    }
    return false;
}


bool Camera::moveUp(int px){
    if((camera.y - px) > -1){
        camera.y -= px;
        return true;
    }
    return false;

}

bool Camera::moveDown(int px){
     if((camera.y + camera.h + px) < level.h+1){
         camera.y += px;
         return true;
     }
     return false;
 }

bool Camera::moveLeft(int px){
    if((camera.x - px) > 0){
        camera.x -= px;
        return true;
    }
    return false;
}

SDL_Rect Camera::getCameraPosition(){
    return camera;
}


bool Camera::moveRight(int px){
    if((camera.x + camera.w) < level.w){
        camera.x += px;
        return true;
    }
    return false;
}

SDL_Rect Camera::updatePosition(SDL_Rect rect, Camera::Speed speed){
    float multiplier;
    switch(speed){
    case Camera::Speed::SLOWEST:
        multiplier = 0.4;
        break;

    case Camera::Speed::SLOWER:
        multiplier = 0.6;
        break;

    case Camera::Speed::SLOW:
        multiplier = 0.8;
        break;

    case Camera::Speed::NORMAL:
        multiplier = 1.0;
        break;

    case Camera::Speed::FAST:
        multiplier = 1.4;
        break;

    case Camera::Speed::FASTER:
        multiplier = 1.6;
        break;

    case Camera::Speed::FASTEST:
        multiplier = 1.8;
        break;

    default:
        multiplier = 1.0;
        break;
    }

    rect.x -= GameUtils::roundToInt(camera.x*multiplier);
    rect.y -= GameUtils::roundToInt(camera.y*multiplier);
    return rect;
}
