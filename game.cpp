#include <SDL_image.h>
#include <SDL_mixer.h>
#include <SDL_ttf.h>
#include <cstdio>
#include "gameutils.h"
#include "gameconfig.h"
#include "animationlibrary.h"
#include "timer.h"
#include "game.h"

Game::Game() {
    renderer = NULL;
    window = NULL;
}

Game::~Game(){
    // Removing renderer and window, also referenced in manager.
    SDL_DestroyRenderer(renderer);
    renderer = NULL;
    SDL_DestroyWindow(window);
    window = NULL;
    // Tell the manager we're done!
    manager.reset();

    printf("Shutting down subsystems...\n");
    // Close IMAGE support
    IMG_Quit();

    // Close Audio
    Mix_CloseAudio();
    while(Mix_Init(0)){
        Mix_Quit();
    }
    // Closing TTF engine.
    TTF_Quit();


    SDL_Quit();
    printf("Thank you for playing. Have a nice day!\n");
}

bool Game::init(){
    printf(ANSI_COLOR_CYAN  "[INITIALIZING]\n" ANSI_COLOR_RESET);

    if(!initVideoSupport()){
        return false;
    }

    if(!initSoundSupport()){
        return false;
    }

    if(!initImageSupport()){
        return false;
    }
    if(!initFontSupport()){
        return false;
    }

    if(!initPhysFS()){
        return false;
    }



    return true;
}


bool Game::initPhysFS(){
    printf("Initializing PhysicsFS... ");
    printf(ANSI_COLOR_YELLOW "PASS!\n" ANSI_COLOR_RESET);
    return true;

}

bool Game::initVideoSupport(){

    printf("Initializing SDL2 and video... ");

    if(SDL_Init(SDL_INIT_VIDEO) < 0){
        printf(ANSI_COLOR_RED "FAILED!\n" ANSI_COLOR_RESET);
        printf("%s\n", SDL_GetError());
        return false;
    }

    int wFlags = SDL_WINDOW_SHOWN;
    if(FULLSCREEN){
        wFlags = SDL_WINDOW_SHOWN | SDL_WINDOW_FULLSCREEN_DESKTOP;
    }

    window = SDL_CreateWindow(
        GAME_TITLE,
        SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED,
        SCREEN_WIDTH,
        SCREEN_HEIGHT,
        wFlags
    );

    if(window == NULL){
        printf(ANSI_COLOR_RED "FAILED!\n" ANSI_COLOR_RESET);
        printf("%s\n", SDL_GetError());
        return false;
    }

    renderer = SDL_CreateRenderer(
        window,
        -1,
        SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC
    );
    if(renderer == NULL){
        printf(ANSI_COLOR_RED "FAILED!\n" ANSI_COLOR_RESET);
        printf("%s\n", SDL_GetError());
        return false;
    }

    SDL_RenderSetLogicalSize(renderer, SCREEN_WIDTH, SCREEN_HEIGHT);

    printf(ANSI_COLOR_YELLOW "\n[EMULATING SCREEN AS %dx%d 32bpp]\n" ANSI_COLOR_RESET,
           SCREEN_WIDTH,
           SCREEN_HEIGHT
    );

    // Set render color to Black
    SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 0xff);

    printf(ANSI_COLOR_GREEN "SUCCESS!\n" ANSI_COLOR_RESET);
    return true;

}

bool Game::initSoundSupport(){
    printf("Initializing Audio... ");
    int flags = MIX_INIT_OGG;
    if(!(Mix_Init(flags) & flags)){
        printf(ANSI_COLOR_RED "FAILED!\n" ANSI_COLOR_RESET);
        printf("%s\n", Mix_GetError());
        return false;
    }
    int audio_rate = 22050;
    Uint16 audio_format = AUDIO_S16; /* 16-bit stereo */
    int audio_channels = 2;
    int audio_buffers = 1024;

    if(Mix_OpenAudio(audio_rate, audio_format, audio_channels, audio_buffers) < 0) {
        printf(ANSI_COLOR_RED "FAILED!\n" ANSI_COLOR_RESET);
        printf("%s\n", Mix_GetError());
        return false;
    }

    Mix_AllocateChannels(24);
    printf(ANSI_COLOR_GREEN "SUCCESS!\n" ANSI_COLOR_RESET);
    return true;
}

bool Game::initFontSupport(){
    printf("Initializing font... ");
    if(TTF_Init() == -1){
        printf(ANSI_COLOR_RED "FAILED!\n" ANSI_COLOR_RESET);
        printf("%s\n", TTF_GetError());
        return false;
    }
    printf(ANSI_COLOR_GREEN "SUCCESS!\n" ANSI_COLOR_RESET);
    return true;
}

bool Game::initImageSupport(){
    printf("Initializing image loading... ");
    int imgFlags = IMG_INIT_PNG | IMG_INIT_JPG;
    if(!(IMG_Init(imgFlags) & imgFlags)){
        printf(ANSI_COLOR_RED "FAILED!\n" ANSI_COLOR_RESET);
        printf("%s\n", IMG_GetError());
        return false;
    }
    printf(ANSI_COLOR_GREEN "SUCCESS!\n" ANSI_COLOR_RESET);
    return true;
}

void Game::run(){
    // Set up some "stuff"
    initGame();
    printf(ANSI_COLOR_CYAN "\n\b[STARTING GAME]\n" ANSI_COLOR_RESET);

    while(GameState::stateId != GameState::State::QUIT){
        runState(GameState::getNewGameState(GameState::stateId, manager));
    }

}

void Game::initGame(){
    GameState::stateId = GameState::State::TEST;
    manager = std::shared_ptr<Manager>(new Manager(window, renderer));
    std::shared_ptr<AnimationLibrary> animLib(new AnimationLibrary());
    manager->setAnimLib(animLib);
}

void Game::runState(std::unique_ptr<GameState> state){
    // Convert the FPS to actual ms.
    Uint32 fpsMS = GameUtils::roundToInt(1000/FPS);
    // We would like to use a correct frame rate.
    Timer t = Timer();
    SDL_Event event;
    while(state->running){
        t.reset();
        t.start();

        // LISTEN
        while(SDL_PollEvent(&event)){
            if(event.type == SDL_KEYDOWN){
                if(event.key.keysym.sym == SDLK_F11){
                    switchVideoMode();
                }
            }
        }

        // PASS ON THE EVENT TO STATE, and it's objects.
        state->listen(event);

        // UPDATE STATE
        state->update();

        // RENDER STATE
        state->render();
        // Calculate how much time the logic, render has taken, minus the delay time.
        Uint32 delay = fpsMS;
        delay = fpsMS - t.getTicks();

        /* Do not allow minus delay, will hang the game.
           Need to typecast to signed int to check whether
           the delay has a negative value.
        */
        delay = ((int) delay < 0) ? 0 : delay;
        SDL_Delay(delay);
    }
}

void Game::switchVideoMode(){
    Uint32 wFlags = SDL_GetWindowFlags(window);
    if(wFlags &= SDL_WINDOW_FULLSCREEN_DESKTOP){
        SDL_SetWindowFullscreen(window, 0);
    }
    else {
        SDL_SetWindowFullscreen(window, SDL_WINDOW_FULLSCREEN_DESKTOP);
    }
}
