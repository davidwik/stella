#ifndef __TIMER_H__
#define __TIMER_H__

class Timer {

public:
    enum class Status { RUNNING, STOPPED, PAUSED };
    Timer();
    void start();
    void stop();
    void pause();
    void reset();
    Uint32 getTicks();

private:
    Uint32 startTime;
    Uint32 savedTime;
    Timer::Status status;

};

#endif /* __TIMER_H__ */
