#ifndef __TESTSTATE_H__
#define __TESTSTATE_H__
#include <memory>
#include "gamestate.h"
#include "animationlibrary.h"


#define SPEEDUP 2000



class TestState : public GameState {

private:
    int counter;
    std::unique_ptr<AnimationLibrary> lib;
    SDL_Texture* background;
    SDL_Texture* fog;

public:
    void listen(SDL_Event event);
    void update();
    void render();
    TestState(std::shared_ptr<Manager> man);
    ~TestState();
};

#endif /* __TESTSTATE_H__ */
