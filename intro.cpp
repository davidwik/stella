#include <cstdio>
#include <string>
#include "intro.h"

Intro::Intro(std::shared_ptr<Manager> man) : GameState(man) {
    name = "Intro";
    counter = 0;
    running = true;
    printf("[%s] - Setting up resources\n", name.c_str());
}

void Intro::listen(SDL_Event event){
    printf("[%s] - Is listening to events!\n", name.c_str());
}

void Intro::update(){
    printf("[%s] - Update objects data.\n", name.c_str());
    counter++;
    if(counter > 10){
        running = false;
        GameState::stateId = GameState::State::MENU;
    }
}

void Intro::render(){
    printf("[%s] - Renders all the data on screen.\n", name.c_str());
}

Intro::~Intro(){
    printf("[%s] - Destroys state\n", name.c_str());
}
