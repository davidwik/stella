#ifndef __GAME_H__
#define __GAME_H__
#include <SDL.h>
#include <memory>
#include "gamestate.h"

class Game {

private:
    SDL_Renderer* renderer;
    SDL_Window* window;
    std::shared_ptr<Manager> manager;

protected:
    bool initVideoSupport();
    bool initSoundSupport();
    bool initImageSupport();
    bool initFontSupport();
    bool initPhysFS();
    void initGame();
    void switchVideoMode();

public:
    void run();
    bool init();
    void runState(std::unique_ptr<GameState> state);
    Game();
    ~Game();
};

#endif /* __GAME_H__ */
