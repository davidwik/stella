#include "teststate.h"
#include "osdir.h"
#include "player.h"
#include "gameutils.h"


TestState::TestState(std::shared_ptr<Manager> man) : GameState(man) {
    SDL_ShowCursor(0);
    int length = 2560*5;

    camera = std::unique_ptr<Camera> (new Camera(length, 1280));
    camera->setMargins();
    if(!man->animLib()->has("enemy")){
        std::shared_ptr<Animation> a (new Animation(man->renderer(), 6));
        a->addFrame("assets/gfx/ships/sweeper/sweeper001.png");
        a->addFrame("assets/gfx/ships/sweeper/sweeper002.png");
        a->addFrame("assets/gfx/ships/sweeper/sweeper003.png");
        a->addFrame("assets/gfx/ships/sweeper/sweeper004.png");
        a->addFrame("assets/gfx/ships/sweeper/sweeper005.png");
        a->addFrame("assets/gfx/ships/sweeper/sweeper006.png");
        a->addFrame("assets/gfx/ships/sweeper/sweeper007.png");
        man->animLib()->add("enemy", a);
    }

    name = "Test";
    counter = 0;
    running = true;
    printf("[%s] - Setting up resources\n", name.c_str());

    Player::init(man);

    background = GameUtils::loadTexture(man->renderer(), "assets/gfx/backgrounds/testback.jpg");
    fog = GameUtils::loadTexture(man->renderer(), "assets/gfx/backgrounds/fog.png");
    gameObjectList.push_back(
        std::shared_ptr<GameObject>(new Player(
        GameObject::Type::PLAYER,
        man,
        200, 200
    )));
}



void TestState::listen(SDL_Event event){

    if(event.type == SDL_QUIT){
        GameState::stateId = GameState::State::QUIT;
        running = false;
    }
    if(event.type == SDL_KEYDOWN){
        if(event.key.keysym.sym == SDLK_ESCAPE){
            GameState::stateId = GameState::State::QUIT;
            running = false;
        }
    }


    for(auto it = gameObjectList.begin();
            it != gameObjectList.end();
        it++){
        (*it)->listen(event, camera);
    }
}

void TestState::update(){
    SDL_Rect r = camera->getCameraPosition();
    printf("Camera x: %d\n", r.x);
    printf("Camera y: %d\n", r.y);

    printf("Camera bottom%d\n", r.y+r.h);

}

void TestState::render(){
    int bWidth = 2560;
    int bHeight = 1280;

    int vel = 0;
    if(timer.getTicks() > SPEEDUP){
        vel = 4;
    }
    camera->moveRight(vel);

    SDL_RenderClear(manager->renderer());

    for(int i = 0; i < 10; i++){
        SDL_Rect back;
        back.x = bWidth*i;
        back.y = 0;
        back.w = bWidth;
        back.h = bHeight;
        SDL_Rect pos = camera->updatePosition(back, Camera::Speed::SLOWEST);
        SDL_RenderCopy(manager->renderer(), background, NULL, &pos);
    }

    for(auto it = gameObjectList.begin();
        it != gameObjectList.end();
        it++){
        (*it)->render();
    }

    if(manager->animLib()->has("enemy")){

        SDL_Rect e;
        e.y = 610;

        for(int i = 1; i < 20; i++){
            e.w = 64;
            e.h = 64;
            e.x = 1280*i;


            if(e.y < 612){
                e.y += i*10;
            }

            if(e.y > 1000){
                e.y -= i*10;
            }

            SDL_Rect ec = camera->updatePosition(e, Camera::Speed::FASTEST);


            SDL_RenderCopy(manager->renderer(),
                           manager->animLib()->get("enemy")->getFrame(),
                           NULL,
                           &ec
            );
        }
    }

    for( int i = 0; i < 10; i++){
        SDL_Rect f;
        f.x = 2560*i;
        f.y = 0;
        f.w = 1280;
        f.h = 1280;
        SDL_Rect n = camera->updatePosition(f, Camera::Speed::SLOW);
        SDL_RenderCopy(manager->renderer(), fog, NULL, &n);
    }

    SDL_RenderPresent(manager->renderer());

}

TestState::~TestState(){
    SDL_DestroyTexture(background);
    SDL_DestroyTexture(fog);
    SDL_ShowCursor(1);
    printf("[%s] - Destroys state\n", name.c_str());
}
