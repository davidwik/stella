#ifndef __CAMERA_H__
#define __CAMERA_H__
#include <SDL.h>
#include "gameconfig.h"
class Camera {

private:
    struct padding {
        int top;
        int bottom;
        int left;
        int right;
    } margins;

    int x = 0;
    int y = 0;
    SDL_Rect level;
    SDL_Rect camera;
    const unsigned short padding = 150;

public:

    enum class Movement { UP, DOWN, LEFT, RIGHT};

    enum class Speed { SLOWEST, SLOWER, SLOW, NORMAL, FAST, FASTER, FASTEST };

    Camera(unsigned int levelWidth,
           unsigned int levelHeight,
           unsigned int cameraWidth = SCREEN_WIDTH,
           unsigned int cameraHeight = SCREEN_HEIGHT
    );

    void setMargins(int top = 128, int bottom = 128, int left = 64, int right = 360);

    bool withinConstraints(SDL_Rect box, Camera::Movement, int m = 0);

    bool moveCameraUp(SDL_Rect box, int px = 10);
    bool moveCameraDown(SDL_Rect box, int px = 10);
    bool moveCameraLeft(SDL_Rect box, int px = 10);
    bool moveCameraRight(SDL_Rect box, int px = 10);

    bool moveUp(int px = 10);
    bool moveDown(int px = 10);
    bool moveLeft(int px = 10);
    bool moveRight(int px = 10);
    SDL_Rect updatePosition(SDL_Rect box, Camera::Speed layer);

    SDL_Rect getCameraPosition(void);
};

#endif /* __CAMERA_H__ */
