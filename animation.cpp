#include "gameutils.h"
#include "animation.h"

Animation::Animation(SDL_Renderer* rend, int framesPerSecond, unsigned int reserved){
    renderer = rend;
    fps = framesPerSecond;
    fps_ms = (int) 1000/fps;
    status = Animation::Status::PLAY;
    frames.reserve(reserved);
    frameDelay.reserve(reserved);
    ts.start();
    currentFrame = 0;
}


Animation::~Animation(){
    for(auto it = frames.begin();
        it != frames.end();
        it++){
        printf("Destroying frame!\n");
        SDL_DestroyTexture(*it);
        *it = NULL;
    }

    frames.clear();
}


void Animation::addFrame(std::string path, float delay){
    SDL_Texture* s;
    s = GameUtils::loadTexture(renderer, path);
    if(s == NULL){
        printf("Failed to load...");
        exit(1);
    }
    else {
        frames.push_back(GameUtils::loadTexture(renderer, path));
        frameDelay.push_back(delay);
    }
}


SDL_Texture* Animation::getFrame(void){
    // get the current frame's delay.
    float delay = frameDelay[currentFrame];
    if(ts.getTicks() > (int) fps_ms*delay){
        currentFrame++;
        ts.reset();
        ts.start();
    }
    // If current frame is out of bounds set it to null.
    currentFrame = (currentFrame > frames.size()-1) ? 0 : currentFrame;
    return frames[currentFrame];
}
