#include <cstdio>
#include <cstdlib>

#include "gamestate.h"
#include "intro.h"
#include "menu.h"
#include "teststate.h"

GameState::State GameState::stateId;

GameState::GameState(std::shared_ptr<Manager> man){
    manager = man;
    timer.start();

}

std::unique_ptr<GameState> GameState::getNewGameState(
    GameState::State st,
    std::shared_ptr<Manager> man){
    switch(st){
    case GameState::State::INTRO:
        return std::unique_ptr<GameState> (new Intro(man));
        break;
    case GameState::State::MENU:
        return std::unique_ptr<GameState> (new Menu(man));
        break;
    case GameState::State::TEST:
        return std::unique_ptr<GameState> (new TestState(man));
        break;
    default:
        printf("Unknown gamestate, panicing.. quitting..");
        exit(1);

    }
}
