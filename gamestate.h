#ifndef __GAMESTATE_H__
#define __GAMESTATE_H__
#include <SDL.h>
#include <memory>
#include <string>
#include <vector>
#include "manager.h"
#include "gameobject.h"
#include "camera.h"
#include "timer.h"

class GameState {

protected:
    Timer timer;
    std::string name;
    std::unique_ptr<Camera> camera;
    std::shared_ptr<Manager> manager;
    std::vector<std::shared_ptr<GameObject> > gameObjectList;

public:
    bool running;

    GameState(std::shared_ptr<Manager> man);

    enum class State {
        INTRO,
        MENU,
        LEVEL,
        TEST,
        QUIT
    };

    static GameState::State stateId;

    virtual void listen(SDL_Event event) = 0;
    virtual void update() = 0;
    virtual void render() = 0;
    virtual ~GameState(){};

    static std::unique_ptr<GameState> getNewGameState(
        GameState::State st,
        std::shared_ptr<Manager> man
    );
};

#endif /* __GAMESTATE_H__ */
