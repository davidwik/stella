#ifndef __PLAYER_H__
#define __PLAYER_H__
#include "gameobject.h"

class Player : public GameObject {

private:
    int moveSpeed = 10;

public:
    Player(GameObject::Type type,
           std::shared_ptr<Manager> man,
           int x = 0,
           int y = 0
    );
    ~Player();
    static void init(std::shared_ptr<Manager> man);

    void listen(SDL_Event ev, std::unique_ptr<Camera> &c);
    void update();
    void render();
};


#endif /* __PLAYER_H__ */
