#include "game.h"

int main(int argc, char* argv[]){
    Game game = Game();

    if(!game.init()){
       printf("Failed to initialize game :(\nQuitting...\n");
    }
    else {
        game.run();
    }
    printf("Exit main!\n");
    return 0;
}
